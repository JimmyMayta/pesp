#include <Servo.h>
#include <SoftwareSerial.h>

const int RX = 10;
const int TX = 11;

Servo servoMotor;
SoftwareSerial BT(RX, TX);

const int Trigger = 2;
const int Echo = 3;
const int Motor = 5;

char cadena[255];
int i = 0;

void setup() {
  BT.begin(9600);
  Serial.begin(9600);
  pinMode(Trigger, OUTPUT);
  pinMode(Echo, INPUT);
  digitalWrite(Trigger, LOW);
  servoMotor.attach(Motor);
  servoMotor.write(0);
}

void loop() {
  if(BT.available()) {
    char dato = BT.read();
    cadena[i++] = dato;

    if(dato == '\n') {
      if(strstr(cadena, "Abrir") != 0) {
        servoMotor.write(120);
        Serial.print(strstr(cadena, "Abrir"));
      }

      if(strstr(cadena, "Cerrar") != 0) {
        servoMotor.write(0);
        Serial.print(strstr(cadena, "Cerrar"));
      }

      BT.write('\r');
      clean();
    }
  }
}

void clean() {
  for(int c1 = 0; c1 <= i; c1++) {
    cadena[c1] = 0;
  }
  i = 0;
}
